package configuration;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Object configuring this application and providing part of an url for rest apis
 */
@ApplicationPath("/api")
public class ApplicationConfig extends Application {

}
