package client.view;

import client.model.Client;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Optional;

/**
 * DAO service for clients, also issuing
 */
@Stateless
@Path("clients")
public class ClientService {

    /**
     * declaring
     */
    @PersistenceContext
    private EntityManager em;

    /**
     * method for finding a single client by his ID in the database
     * also a rest API for getting that client
     * @param id
     * @return
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Client findClient (@PathParam("id") int id) {
        return em.createQuery("select a from Client a where a.id = :id", Client.class).setParameter("id", id).getSingleResult();
    }

    /**
     * method used to persist a client to the database
     * also a REST api for doing that
     * @param client
     */
    @Path("save")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void saveClient (Client client) {
        em.persist(client);
    }


    /**
     * Method chcecking if a client kwit a specified name and surname exists in the database
     * @param name - name of a client sought in the database
     * @param surname - surname of a client sought in the database
     * @return Optional Client - if a client exists returns that client if not, returns an empty optional
     */
    public Optional<Client> findClientbyName (final String name, final String surname) {
        TypedQuery<Client> query = em.createQuery("select a from Client a where a.name = :name and a.surname = :surname", Client.class);
        query.setParameter("name", name);
        query.setParameter("surname", surname);
        try {
            return Optional.of(query.getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }
}
