package client.view;

import antique.model.Antique;
import antique.view.AntiquesService;
import basket.view.BasketView;
import client.model.Client;
import order.model.Order;
import order.view.OrdersService;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

/**
 * bean for managing clients and their order
 */
@Named
@ViewScoped
public class ClientAdd implements Serializable {

    /**
     * Injecting DAO service for clients
     */
    @EJB
    private ClientService clientService;

    /**
     * Injecting a basket bean containing antiques
     */
    @EJB
    private BasketView basketView;

    /**
     * Injecting DAO service for orders
     */
    @EJB
    private OrdersService ordersService;

    /**
     * Injecting DAO service for antiques
     */
    @EJB
    private AntiquesService antiquesService;

    private Client client = new Client();

    /**
     * method ivoing a method from DAO for antiques, persisting a client
     * @param client
     */
    public Client addNewClient(Client client) {
        clientService.saveClient(client);
        return client;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Method invoked in the basket view creating an order for each of the antiques in basket
     * also for each order, sets the client that is provided in the form
     * method performs a check on the database if the client exists,
     * if a client does not exist it is created and saved in the database when user submits an order
     *
     */
    public void submitOrders () {
        Optional<Client> clientbyName = clientService.findClientbyName(client.getName(), client.getSurname());
        if (clientbyName.isPresent()) {
            client = clientbyName.get();
        } else {
            client = addNewClient(client);
        }

        for (Antique antique:basketView.getAntiquesInBasket()) {
            antique.setAvailability(Antique.Availability.UNAVAILABLE);
            antiquesService.upadeAntique(antique);
            Order order = new Order();
            order.setAntique(antique);
            order.setClient(client);
            ordersService.saveOrder(order);
        }
        basketView.getAntiquesInBasket().clear();

    }
}
