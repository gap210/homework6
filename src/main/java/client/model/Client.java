package client.model;


import javax.persistence.*;
import java.util.Objects;

/**
 * Object representing a client
 * client has a name, a surname and if persisted in the database, has an id
 * name and surname are unique to each client
 */
@Entity
@Table(name = "clients", uniqueConstraints = @UniqueConstraint(columnNames = {"name", "surname"}))
public class Client {

    @Column
    private String name;

    @Column
    private String surname;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public Client() {
    }

    public Client(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id &&
                Objects.equals(name, client.name) &&
                Objects.equals(surname, client.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, id);
    }
}
