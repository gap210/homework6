package country.view;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;


/**
 * Converter for the country object to be diplayed in facelets
 */
@FacesConverter("countriesConverter")
public class CountryConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        CountryService service = CDI.current().select(CountryService.class).get();
        if (value == null || value.equals(null)) {
            return null;
        }
        return service.findCountries().get(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        CountryService service = CDI.current().select(CountryService.class).get();

        return service.findCountries().indexOf(value) + "";
    }
}
