package country.view;

import country.model.Country;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * DAO  service object for the countries table and country object
 * also issues REST apis for countries
 */
@Stateless
@Path("countries")
public class CountryService {

    @PersistenceContext
    private EntityManager em;

    /**
     * method returning all countries objects saved in the database
     * @return List<Country>
     */
    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Country> findCountries () {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Country> query = cb.createQuery(Country.class);
        query.from(Country.class);
        List<Country> countriesList = em.createQuery(query).getResultList();
        return countriesList;
    }

    /**
     * method used to save a country to the database, also a REST api to do the same
     * @param country
     */
    @Path("")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void saveCountry (Country country) {

        em.persist(country);
    }
}
