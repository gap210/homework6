package country.model;

import antique.model.Antique;

import javax.persistence.*;
import java.util.*;

/**
 * Object representing a country
 * country has a nam and id (if persisted int he database)
 */
@Entity
@Table(name = "countries")
public class Country {


    @Column
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    

    public Country() {
    }

    public Country(String name) {
        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Objects.equals(name, country.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, id);
    }

    @Override
    public String toString() {
        return name + "";
    }
}
