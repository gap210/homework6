package basket.view;

import antique.model.Antique;
import antique.view.AntiquesService;
import client.model.Client;
import client.view.ClientAdd;
import client.view.ClientService;
import order.model.Order;
import order.view.OrdersService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean for storing and viewing items that are in the basket
 */
@Named
@ApplicationScoped
@Stateless
public class BasketView implements Serializable {




    private List<Antique> antiquesInBasket = new ArrayList<>();

    /**
     * method returning all the antiques that are currently in the basket
     * @return
     */
    public List<Antique> getAntiquesInBasket() {

        return antiquesInBasket;
    }

    /**
     * Method setting a list of antiques to a list of antiques in the basket
     * @param antiquesInBasket
     */
    public void setAntiquesInBasket(List<Antique> antiquesInBasket) {

        this.antiquesInBasket = antiquesInBasket;
    }

    /**
     * method for adding an antique to basket
     * @param antique - antique to be added to the basket
     */
    public void addAntiqueToBasket(Antique antique) {
        if (!antiquesInBasket.contains(antique)) {
            antiquesInBasket.add(antique);
        }
    }


}

