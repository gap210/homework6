package antique.view;

import antique.model.Antique;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Optional;

/**
 * Bean for servicing antiques (DAO) also issuing REST apis for antiques
 */
@Stateless
@Path("antiques")
public class AntiquesService {


    /**
     * defining an entity manager
     */
    @PersistenceContext
    private EntityManager em;


    /**
     * Method returning all the antiques saved in the database
     * @return List<Antiques>
     */
    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Antique> findAntiques() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Antique> query = cb.createQuery(Antique.class);
        query.from(Antique.class);
        List<Antique> antiquesList = em.createQuery(query).getResultList();
        return antiquesList;
    }

    /**
     * Method returning an antique from the database by its id
     * @param id - id of an antique
     * @return Antique
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Antique findAntique(int id) {
        Optional<Antique> antique = Optional.ofNullable(em.find(Antique.class, id));
        if (antique.isPresent()) {
            em.detach(antique.get());
            return antique.get();
        } else {
            return new Antique();
        }
    }

    /**
     * Method involing persist on an antique
     * @param antique - antique object to be persisted in the database
     */
    @Path("save")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void saveAntique(Antique antique) {
        em.persist(antique);
    }

    /**
     * Method involking merge on the antiqe set as parameter
     * @param antique - antique to be merged
     */
    public void upadeAntique(Antique antique) {
        em.merge(antique);
    }
}
