package antique.view;


import antique.model.Antique;
import country.view.CountryService;
import order.view.OrdersService;

import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean for editing an existing antique
 */
@Named
@ViewScoped
public class AntiqueEdit implements Serializable {

    /**
     * Injecting DAO service object for antiques
     */
    @EJB
    private AntiquesService antiqueService;


    /**
     * injecting DAO service for countries
     */
    @EJB
    private CountryService countryService;

    List<SelectItem> availabilityList;

    private int antiqueId;

    private Antique antique;

    private List<SelectItem> countriesList;

    public int getAntiqueId() {
        return antiqueId;
    }

    public void setAntiqueId(int antiqueId) {
        this.antiqueId = antiqueId;
    }

    /**
     * Method for getting Antique by its id from the database
     * @return Antique
     */
    public Antique getAntique () {
        if (antique == null) {
            antique = antiqueService.findAntique(antiqueId);
        }
        return antique;
    }

    /**
     * Method invoking a merge method from the entity manager on the edited antique
     */
    public void updateAntique () {
        if (antiqueId == 0) {
            antiqueService.saveAntique(antique);

        } else {
            antiqueService.upadeAntique(antique);
        }
    }

    /**
     * Method for getting all the countries from the database
     * to be displayed in the multiple choice list in a facelet
     * @return List<SelectItem>
     */
    public List<SelectItem> getCountries () {
        if (countriesList == null) {
            countriesList = new ArrayList<>();
            countryService.findCountries().forEach(country -> countriesList.add(new SelectItem(country, country.getName())));
        }
        return countriesList;
    }

    /**
     * Method invoking a method persisting an antique from the antiques DAO
     */
    public void saveAntique () {
        antiqueService.saveAntique(antique);
    }

    public List<SelectItem> getAvailability () {
        if (availabilityList == null) {
            availabilityList = new ArrayList<>();
            for (Antique.Availability availability : Antique.Availability.values()) {
                availabilityList.add(new SelectItem(availability, availability.name()));
            }
        }
        return availabilityList;
    }

    /**
     * function used to determin thwether availability can be set (in the add mode)
     * or not (in the edit mode)
     * @return
     */
    public boolean getAvailabilityEditCheck (int id) {
        if (id == 0) {
            return false;
        } else {
            return true;
        }
    }

}
