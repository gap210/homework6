package antique.view;

import antique.model.Antique;
import basket.view.BasketView;
import com.sun.xml.internal.messaging.saaj.soap.impl.CDATAImpl;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean for diplaying information about all antiques in the database
 */
@Named
@ViewScoped
public class AntiqueView implements Serializable {

    /**
     * Injecting DAO service object for antiques
     */
    @EJB
    private AntiquesService service;

    /**
     * Injecting view of a shopping basket of the store
     */
    @Inject
    private BasketView basketView;


    /**
     * Antiques list holding all from the database
     */
    private List<Antique> antiqueList;


    /**
     * method used to populate antiques in the list from the database
      * @return List<Antique>
     */
    public List<Antique> getAntiqueList () {
        if(antiqueList == null) {
            antiqueList = service.findAntiques();
        }
        return antiqueList;
    }

    /**
     * Method used to add an antique from all available to the basket
     * @param antique - antique to be set in basket
     */
    public void addAntiqueToBasketView (Antique antique) {
        if (!basketView.getAntiquesInBasket().contains(antique)) {
            basketView.addAntiqueToBasket(antique);
        }
    }

    /**
     * Method returning all the antiques that are currently in a basket
     * @return
     */
    public List<Antique> getAntiquesFromBasket () {
        return basketView.getAntiquesInBasket();
    }

    /**
     * Method used to add antiques to the basket
     * @param antique - antique to be added to basket
     * @return
     */
//    public boolean addedToBasket (Antique antique) {
//        return (basketView.getAntiquesInBasket().contains(antique) || antique.getAvailability() == Antique.Availability.UNAVAILABLE);
//    }
    public boolean addedToBasket (Antique antique) {
        return (basketView.getAntiquesInBasket().contains(antique) || antique.getAvailability() == Antique.Availability.UNAVAILABLE);
    }



    /**
     * method used to remove antiques from the basket
     * @param antique - antique to be removed from the basket
     */
    public void removeAntiqueFromBasket (Antique antique) {
        basketView.getAntiquesInBasket().remove(antique);
    }



}
