package antique.view;


import antique.model.Antique;
import country.model.Country;
import country.view.CountryService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;


/**
 * One bean to be executed at the start of the application
 * Creating 6 countries and 3 antiques and persisting them to the database
 */

@Singleton
@Startup
public class InitAntiques {

    Country poland = new Country("Poland");
    Country australia = new Country("Australia");
    Country greenland = new Country("Greenland");
    Country norway = new Country("Norway");
    Country sudan = new Country("Sudan");
    Country egypt = new Country("Egypt");

    Antique antique1 = new Antique("Patelnia Królowej",1865, 23500, poland, Antique.Availability.AVAILABLE);
    Antique antique2 = new Antique("Dzida Faraona",87, 1700000, egypt, Antique.Availability.AVAILABLE);
    Antique antique3 = new Antique("Rondelki Ksiezniczki",1468, 4500,norway, Antique.Availability.AVAILABLE);

    /**
     * injecting DAO service for antiques
     */
    @EJB
    private AntiquesService service;

    /**
     * Injecting DAO object for countris
     */
    @EJB
    private CountryService countryService;

    /**
     * Method for persisting previously created objects
     * To be executed after the application builds
     */
    @PostConstruct
    public void init () {

        if ((service.findAntiques() == null || service.findAntiques().isEmpty()) &&
                (countryService.findCountries() == null || countryService.findCountries().isEmpty())) {

            countryService.saveCountry(poland);
            countryService.saveCountry(australia);
            countryService.saveCountry(greenland);
            countryService.saveCountry(norway);
            countryService.saveCountry(sudan);
            countryService.saveCountry(egypt);

            service.saveAntique(antique1);
            service.saveAntique(antique2);
            service.saveAntique(antique3);

        }

    }








}
