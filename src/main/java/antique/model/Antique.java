package antique.model;

import country.model.Country;
import order.model.Order;

import javax.persistence.*;
import java.util.Objects;

/**
 * This is a class representing an Antique
 */
@Entity
@Table(name = "antiques")
public class Antique {

    @Column
    private String name;

    @Column
    private Integer productionDate;

    /**
     * Many to one relation with country ie. there may be many antiques from the same country
     * this is the managing side of the relation
     */
    @ManyToOne( fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id", referencedColumnName = "id")
    private Country originCountry;

    @Column
    private Integer price;


    @Enumerated(EnumType.STRING)
    @Column
    private Availability availability;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    public enum Availability {

        AVAILABLE,
        UNAVAILABLE
    }

    public Antique() {
    }

    public Antique(String name, int productionDate, int price, Country originCountry, Availability availability) {
        this.name = name;
        this.productionDate = productionDate;
        this.price = price;
        this.availability = availability;
        this.originCountry = originCountry;
    }

    public Integer getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Integer productionDate) {
        this.productionDate = productionDate;
    }

    public Country getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(Country originCountry) {
        this.originCountry = originCountry;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Antique antique = (Antique) o;
        return productionDate == antique.productionDate &&
                price == antique.price &&
                id == antique.id &&
                Objects.equals(originCountry, antique.originCountry) &&
                availability == antique.availability;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productionDate, originCountry, price, availability, id);
    }
}
