package order.model;

import antique.model.Antique;
import client.model.Client;

import javax.persistence.*;
import java.util.Objects;

/**
 * bean for an order Object has a client and antique fields
 * OneToOne relation with Antique
 */
@Entity
@Table(name = "orders")
public class Order {

    /**
     * many to ne relation with client ie. one client can have many orders
     * this is the managing side of the relation
     */
    @ManyToOne
    @JoinColumn(name ="client_id", referencedColumnName = "id")
    private Client client;

    /**
     * one to one relaton with antique, done on primary keys of both objects
     */
    @OneToOne
    @PrimaryKeyJoinColumn
    private Antique antique;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public Order() {
    }

    public Order(Client client, Antique antique) {
        this.client = client;
        this.antique = antique;
    }

    public Antique getAntique() {
        return antique;
    }

    public void setAntique(Antique antique) {
        this.antique = antique;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(client, order.client) &&
                Objects.equals(antique, order.antique);
    }

    @Override
    public int hashCode() {
        return Objects.hash(client, antique);
    }
}
