package order.view;

import com.sun.org.apache.xpath.internal.operations.Or;
import country.model.Country;
import order.model.Order;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Stateless
@Path("orders")
public class OrdersService {

    @PersistenceContext
    private EntityManager em;

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> findOrders () {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        query.from(Order.class);
        List<Order> ordersList = em.createQuery(query).getResultList();
        return ordersList;
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Order findOrder ( int id) {
        return em.createQuery("select c from Order c where c.id = :id", Order.class).setParameter("id", id).getSingleResult();
    }


    @Path("")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void saveOrder (Order order) {
        em.persist(order);
    }

    @Path("")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateOrder (Order order) {
        em.merge(order);
    }
}
