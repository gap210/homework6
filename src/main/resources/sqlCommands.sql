CREATE TABLE countries (
  name VARCHAR(256) NOT NULL,
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);

CREATE TABLE antiques (
  productionDate INT NOT NULL,
  price INT NOT NULL,
  availability VARCHAR (256) NOT NULL,
  country_id INT,
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);
ALTER TABLE antiques ADD FOREIGN KEY (country_id) REFERENCES countries (id);


CREATE TABLE clients (
  name VARCHAR (256),
  surname VARCHAR (256),
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);

CREATE TABLE orders (
  client VARCHAR (256) NOT NULL,
  antique VARCHAR (256) NOT NULL,
  client_id INT NOT NULL,
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);
ALTER TABLE orders ADD FOREIGN KEY (id) REFERENCES antiques (id);
ALTER TABLE orders ADD FOREIGN KEY (client_id) REFERENCES clients (id);